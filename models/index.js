const Sequelize = require('sequelize');
const DataloggerModel = require('./datalogger');
const EventsListModel = require('./eventslist');
const EventsModel = require('./events');

const sequelize = new Sequelize('user1', 'user1', 'user1', {
  host: '127.0.0.1',
  dialect: 'mysql',
  pool: {
    min: 0,
    max: 5,
    idle: 10000,
    acquire: 40000,
    evict: 20000,
    concurrency: 10,
    maxIdleTime: 120000,
  },
  timezone: '+02:00',
  logging: true,
  dialectOptions: {
    multipleStatements: true,
  },
});

const Datalogger = DataloggerModel(sequelize, Sequelize);
const EventsList = EventsListModel(sequelize, Sequelize);
const Events = EventsModel(sequelize, Sequelize);

EventsList.hasMany(Events);
Events.belongsTo(EventsList);


sequelize.sync();

module.exports = {
  Datalogger,
  EventsList,
  Events,
  sequelize,
};
