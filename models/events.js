module.exports = (sequelize, DataTypes) => {
  const Events = sequelize.define('Events', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    value: DataTypes.STRING,
    timestamp: DataTypes.DATE(3),
  }, {});

  return Events;
};
