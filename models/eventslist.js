module.exports = (sequelize, DataTypes) => {
  const EventsList = sequelize.define('EventsList', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: DataTypes.STRING,
    type: DataTypes.ENUM('DETECT', 'COUNT'),
  }, {});

  return EventsList;
};
