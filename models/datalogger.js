module.exports = (sequelize, DataTypes) => {
  const Datalogger = sequelize.define('Datalogger', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: DataTypes.STRING,
    numericid: DataTypes.INTEGER,
    value: DataTypes.STRING,
    derivate: DataTypes.INTEGER,
    timestamp: DataTypes.DATE(3),
    ref: DataTypes.DATE(3),
    quality: DataTypes.INTEGER,
  }, {
    timestamp: false,
  });
  return Datalogger;
};
