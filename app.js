const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

<<<<<<< HEAD
var indexRouter = require('./routes/index');
var resultsRouter = require('./routes/results');
var dataloggerRouter = require('./routes/datalogger');
=======
>>>>>>> develop

const indexRouter = require('./routes/index');
const dataloggerRouter = require('./routes/datalogger');
const snapshotRouter = require('./routes/snapshot');
const eventslistRouter = require('./routes/eventslist');
const eventsRouter = require('./routes/events');
const exportRouter = require('./routes/export');
const transposeRouter = require('./routes/transpose');


const app = express();

app.use((req, res, next) => {
  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3001');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/js', express.static(`${__dirname}/node_modules/bootstrap/dist/js`)); // redirect bootstrap JS
app.use('/js', express.static(`${__dirname}/node_modules/jquery/dist`)); // redirect JS jQuery
app.use('/js', express.static(`${__dirname}/node_modules/slick-carousel/slick`)); // redirect JS jQuery
app.use('/css', express.static(`${__dirname}/node_modules/bootstrap/dist/css`)); // redirect CSS bootstrap
app.use('/cssVis', express.static(`${__dirname}/node_modules/vis/dist`)); // redirect CSS bootstrap

app.use('/', indexRouter);
app.use('/results', resultsRouter);
app.use('/datalogger', dataloggerRouter);
app.use('/eventslist', eventslistRouter);
app.use('/events', eventsRouter);
app.use('/export', exportRouter);
app.use('/transpose', transposeRouter);


// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// error handler
app.use((err, req, res) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
