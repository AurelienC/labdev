import random
import sys
import time
import socket
import cv2
from matplotlib import pyplot as plt
import datetime
from threading import Thread
import threading


class Snapshot(Thread):
    def __init__(self, qty, interval, ip, portOpc, portHmi, event):
        Thread.__init__(self)
        self.qty = qty
        self.interval = interval
        self.ip = ip
        self.portOpc = portOpc
        self.portHmi = portHmi
        self.event = event


    def run(self):
        # Initialisation variables
        i = 0
        sockTx = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # Serveur OPC
        cam = cv2.VideoCapture(0) # Webcam


        listTime = list()
        start = time.process_time()
        last = start
        prec = last

        # Start to IHM
        MESSAGE = "start," + str(0)
        sockTx.sendto(bytes(MESSAGE, "utf-8"), (self.ip, self.portHmi))

        while i < valN and not self.event.is_set():
            i += 1

            ## Merisation de l'heure
            n = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")
            t = int(time.time()*1000)

            # Image to IHM
            MESSAGE = str(i) + "," + str(t)
            sockTx.sendto(bytes(MESSAGE, "utf-8"), (self.ip, self.portHmi))

            # OPC
            MESSAGE = str(t) + "\n" # [1, t]
            sockTx.sendto(bytes(MESSAGE, "utf-8"), (self.ip, self.portOpc))

            ret_val, img = cam.read()

            if ret_val!= True:
                raise ValueError("Can't read frame")

            font = cv2.FONT_HERSHEY_SIMPLEX
            cv2.putText(img, n ,(0,20), font, 0.75, (0,0,0), 2, cv2.LINE_AA)
            cv2.imwrite('D:\\git\\labDev\\public\\images\\img' + str(t) + '.jpg', img)

            # print("img" + str(t) + "  ---  " + n + " - " + str(last-prec))
            if i > 2 :
                listTime.append(last-prec)

            while (last + interval) > time.process_time():
               pass
            prec = last
            last = time.process_time()

        # print("MIN: " + str(min(listTime)) + " --- MAX: " + str(max(listTime)) + " --- AVG: " + str(sum(listTime) / len(listTime)))
        cam.release()
        cv2.destroyAllWindows()

        # End to IHM
        MESSAGE = "stop" + "," + str(0)
        sockTx.sendto(bytes(MESSAGE, "utf-8"), (self.ip, self.portHmi))


class Polling(Thread):
    def __init__(self, ip, port, eventPoll, eventSnap):
        Thread.__init__(self)
        self.ip = ip
        self.port = port
        self.eventPoll = eventPoll
        self.eventSnap = eventSnap

    def run(self):
        sockRx = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
        sockRx.bind((self.ip, self.port))
        sockRx.setblocking(0)

        while not self.eventPoll.is_set():
            try:
                data, addr = sockRx.recvfrom(1024)
            except socket.error:
                data = None

            if data is not None:
                # print("[POLL] data received")
                self.eventSnap.set()
                break

# Run server
# print("Start")

# Modify values
UDP_IP = "127.0.0.1"
UDP_PORT_OPC = 2101
UDP_PORT_HMI = 33333
UDP_PORT_CANCEL = 2102

if len(sys.argv) == 3:
    valN = int(sys.argv[1])
    interval = float(sys.argv[2])
else:
    valN = 2
    interval = 0.5


eventKillSnap = threading.Event() # event will kill Snap thread
eventKillPoll = threading.Event() # event will kill Poll thread

thread_snap = Snapshot(valN, interval, UDP_IP, UDP_PORT_OPC, UDP_PORT_HMI, eventKillSnap)
thread_poll = Polling(UDP_IP, UDP_PORT_CANCEL, eventKillPoll, eventKillSnap)

thread_snap.start()
thread_poll.start()

thread_snap.join()
eventKillPoll.set()
thread_poll.join()

# print("END")



## Attente reception donnees
# sockRx.setblocking(1)
# data, addr = sockRx.recvfrom(1024)
# val = "".join(map(chr, data))
# valN = int(val)
