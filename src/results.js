const angular = require('angular');
// const Bootstrap = require('bootstrap');
const Highcharts = require('highcharts');
<<<<<<< HEAD
const _ = require('underscore');


const app = angular.module('results', []);

app.controller('ctrl', ($scope, $http) => {
=======
const io = require('socket.io-client');
const moment = require('moment');
// const vis = require('vis');
const _ = require('underscore');
// const minimongo = require('minimongo');

require('angular-moment');
require('highcharts/modules/exporting')(Highcharts);
require('highcharts/modules/export-data')(Highcharts);


const app = angular.module('res1');


// var LocalDb = minimongo.MemoryDb;
// const db = new LocalDb();
// db.addCollection('dl');

app.controller('ctrlResults', ($scope, $http, $q) => {
>>>>>>> develop
  function getApi(url, params = {}, callback) {
    return $http.get(url, { params }).then(data => callback(data));
  }

<<<<<<< HEAD
  function getTags() {
    return getApi('/datalogger/tags', {}, (d) => {
      $scope.tags = _.map(d.data, obj => _.extend(obj, { visible: false }));
    });
  }

  function getUdp(param, callback) {
    return {
      param,
      callback,
    }; // $http.get(`udp.php?req=${param}`).then(data => callback(data));
  }

  function getImage(ts) {
    return getApi('/datalogger/images', { timestamp: ts }, (d) => {
      console.log(d);
      [$scope.img] = d.data;
    });
  }


  function trace() {
=======
  function getApiSync(url, params = {}) {
    const defer = $q.defer();
    $http.get(url, { params }).then((data) => {
      defer.resolve(data);
    });
    return defer.promise;
  }

  function postApi(url, params = {}, callback) {
    return $http.post(url, { params }).then(data => callback(data));
  }

  // function putApi(url, params = {}, callback) {
  //   return $http.put(url, { params }).then(data => callback(data));
  // }

  function deleteApi(url, params = {}, callback) {
    return $http.delete(url, { params }).then(data => callback(data));
  }

  function getTags() {
    return getApi('/datalogger/tags', {}, (d) => {
      $scope.tags = _.map(d.data, obj => _.extend(obj, { visible: false }));

      // $scope.toDerived($scope.tags);
      // getApi('/datalogger/formatData', {});
    });
  }

  function getImage(ts) {
    getApi('/datalogger/image', { timestamp: moment(ts, 'x').valueOf() }, (d) => {
      if (d.data.length > 0) {
        [$scope.img] = d.data;
        $scope.img.date = moment(d.data[0].y, 'xx').format('HH:mm:ss.SSS');
      } else {
        $scope.img = '';
        $scope.img.date = '';
      }
    });
  }

  function getImages() {
    getApi('/datalogger/images', {}, (d) => {
      $scope.images = d.data;
    });
  }

  function getEventsList() {
    return getApi('/eventslist', {}, (d) => {
      $scope.eventsList = _.map(d.data, obj => _.extend(obj, { visible: false }));
    });
  }


  function updateEventsForImage() {
    getApi('/events', { timestamp: $scope.images[$scope.index].timestamp }, (d) => {
      $scope.eventsForImge = d.data;
    });
    $scope.addEvent.timestamp = $scope.images[$scope.index].timestamp;
  }

  function trace() {
    Highcharts.setOptions({
      global: {
        useUTC: false,
      },
    });

>>>>>>> develop
    $scope.chart = Highcharts.chart('container', {
      chart: {
        type: 'line',
        zoomType: 'x',
      },
      title: {
        text: null,
      },
      xAxis: {
        type: 'datetime',
        startOnTick: true,
<<<<<<< HEAD
      },
      yAxis: {
=======
        crosshair: true,
      },
      yAxis: [{
>>>>>>> develop
        title: {
          text: 'Value',
        },
      },
<<<<<<< HEAD
=======
      {
        title: {
          text: 'Mask',
        },
        min: 0,
        max: 1,
      }],
>>>>>>> develop
      chartOptions: {
        legend: {
          enabled: true,
        },
      },
<<<<<<< HEAD
=======
      credits: {
        enabled: false,
      },
>>>>>>> develop
      legend: {
        align: 'center',
        verticalAlign: 'bottom',
      },
      tooltip: {
        crosshairs: true,
      },
      rangeSelector: {
        floating: true,
        y: -65,
        verticalAlign: 'bottom',
      },
      plotOptions: {
        series: {
<<<<<<< HEAD
          cursor: 'pointer',
          point: {
            events: {
              mouseOver() {
                // $scope.img = this.y;
                getImage(this.x);
                // $scope.$apply();
                console.log($scope.img);
=======
          turboThreshold: 10000,
          cursor: 'pointer',
          point: {
            events: {
              click() {
                getImage(this.ts);
>>>>>>> develop
              },
            },
          },
        },
      },
      series: [],
    });
<<<<<<< HEAD
  }

  getTags();
  trace();

  $scope.img = 1;
=======

    // $scope.chartResult = Highcharts.chart('containerResult', {
    //   chart: {
    //     type: 'bar',
    //   },
    //   title: {
    //     text: 'Results',
    //   },
    //   xAxis: {
    //     type: 'category',
    //     title: {
    //       text: 'Signal',
    //     },
    //   },
    //   yAxis: [{
    //     title: { text: 'Score (expected 1)' },
    //     plotBands: [{ // visualize the weekend
    //       from: 0.5,
    //       to: 1.5,
    //       color: 'rgba(68, 213, 170, .5)',
    //     }],
    //   }, {
    //     title: { text: 'Score (expected 0)' },
    //   }],
    //   plotOptions: {
    //     column: {
    //       stacking: 'normal',
    //       dataLabels: {
    //         enabled: true,
    //         color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
    //       },
    //     },
    //   },
    //   chartOptions: {
    //     legend: {
    //       enabled: false,
    //     },
    //   },
    //   credits: {
    //     enabled: false,
    //   },
    //   legend: {
    //     align: 'center',
    //     verticalAlign: 'bottom',
    //   },
    //   tooltip: {
    //     crosshairs: true,
    //   },
    //   series: [],
    // });
  }

  $scope.outWindow = [];
  function compute(ref, signalsInTheRace, cb) {
    $scope.computeState = 1;
    $scope.chart.xAxis[0].removePlotBand();
    $scope.signalsInTheRace = signalsInTheRace;

    if (ref.length === 0 || signalsInTheRace.length === 0) {
      $scope.computeState = 2;
      cb(signalsInTheRace);
    }

    // find next 0
    const start = _.findIndex(ref, { y: 0 });

    // find next 1 and take the last 0
    const end = start + _.findIndex(_.rest(ref, (start === 0) ? 1 : start), { y: 1 });

    if (start < 0 || end < 0) {
      $scope.computeState = 2;
      cb(signalsInTheRace);
      return;
    }

    $scope.chart.xAxis[0].addPlotBand({
      from: ref[start].x,
      to: ref[end].x,
      color: '#ffc107',
    });

    $scope.outWindow.push({
      start: moment(ref[start].x, 'x').valueOf(),
      end: moment(ref[end].x, 'x').valueOf(),
    });

    getApiSync('/datalogger/compute', {
      start: moment(ref[start].x, 'x').valueOf(),
      end: moment(ref[end].x, 'x').valueOf(),
      signalsInTheRace,
    }).then((d) => {
      compute(_.rest(ref, (end === 0) ? 1 : end), _.pluck(d.data, 'name'), cb);
    });
  }


  function windowWithEvents(w) {
    const wResult = [];

    _.each(w, (e) => {
      getApiSync('/events/window', {
        start: moment(e.start, 'x').valueOf(),
        end: moment(e.end, 'x').valueOf(),
      }).then((d) => {
        wResult.push({
          start: e.start,
          end: e.end,
          events: d,
        });
        $scope.wEventsIn = wResult;
      });
    });
    return wResult;
  }

  // function signalsScoreByWindow(w) {
  //   const wResult = [];
  //
  //   _.each(w, (e, i) => {
  //     _.extend(w[i], {
  //       signal: [],
  //     });
  //
  //     getApiSync('/datalogger/window', {
  //       start: moment(e.start, 'x').valueOf(),
  //       end: moment(e.end, 'x').valueOf(),
  //     }).then((d) => {
  //       let score = 0;
  //       let previous = parseInt(d.data[0].value, 10);
  //
  //       _.each(d.data, (o) => {
  //         if (previous !== parseInt(o.value, 10)) {
  //           score += 1;
  //         }
  //         previous = parseInt(o.value, 10);
  //       });
  //
  //       $scope.wSignalsIn = wResult;
  //     });
  //   });
  //
  //
  //   return wResult;
  // }

  $scope.inWindow = [];
  function compute2(ref, signalsInTheRace, cb) {
    $scope.compute2State = 1;
    $scope.chart.xAxis[0].removePlotBand();
    $scope.signalsInTheRace2 = signalsInTheRace;

    if (ref.length === 0 || signalsInTheRace.length === 0) {
      $scope.compute2State = 2;
      cb(signalsInTheRace);
    }

    // find next 0
    const start = _.findIndex(ref, { y: 1 });

    // find next 1 and take the last 0
    const end = start + _.findIndex(_.rest(ref, (start === 0) ? 1 : start), { y: 0 });

    if (start < 0 || end < 0 || end < start || start === end) {
      $scope.compute2State = 2;
      cb(signalsInTheRace);
      return;
    }

    $scope.chart.xAxis[0].addPlotBand({
      from: ref[start].x,
      to: ref[end].x,
      color: '#28a745',
    });


    $scope.inWindow.push({
      start: moment(ref[start].x, 'x').valueOf(),
      end: moment(ref[end].x, 'x').valueOf(),
    });

    getApiSync('/datalogger/compute2', {
      start: moment(ref[start].x, 'x').valueOf(),
      end: moment(ref[end].x, 'x').valueOf(),
      signalsInTheRace,
    }).then((d) => {
      compute2(_.rest(ref, (end === 0) ? 1 : end), _.pluck(d.data, 'name'), cb);
    });
  }
  // TODO: "2"


  getTags();
  getEventsList();
  trace();
  $scope.images = [];
  $scope.index = 0;
  getImages();


  $scope.mode = false;
  $scope.onProgress = false;
  $scope.imageName = 0;

  // 'http://localhost:3000'
  // $scope.socket = io('http://localhost:3000');
  $scope.socket = io();

  let precImage = '';
  $scope.socket.on('c', (data) => {
    $scope.progress = Number(data.mes.split(',')[0]) / $scope.qty * 100 || 0;
    if (data.mes.split(',')[0] === 'start') $scope.onProgress = true;
    if (data.mes.split(',')[0] === 'stop') $scope.onProgress = false;
    $scope.image = Number(data.mes.split(',')[0]) || 0;
    $scope.imageName = precImage;
    precImage = data.mes.split(',')[1];
    $scope.date = moment(data.mes.split(',')[1], 'xx').format('HH:mm:ss.SSS');
    $scope.$apply();
  });

  // socket.on('c', (data) => {
  //   console.log('socket', data);
  //   // $scope.msg = data.msg;
  // });

  $scope.img = 0;
>>>>>>> develop
  $scope.nb = 5;
  $scope.onlyVisible = {
    visible: false,
  };
<<<<<<< HEAD

  $scope.add = (datalogger) => {
=======
  $scope.ref = false;
  $scope.computeState = 0;

  $scope.addEvent = {
    value: '',
    timestamp: '',
  };

  $scope.windowWithEvents = () => {
    windowWithEvents($scope.inWindow);
  };

  $scope.compute = () => {
    compute($scope.mask, _.pluck($scope.tags, 'name'), (d) => {
      $scope.computeResult = d;
    });
  };

  $scope.compute2 = () => {
    compute2($scope.mask, _.pluck($scope.tags, 'name'), (d) => {
      $scope.compute2Result = d;
    });
  };

  $scope.compute3 = () => {
    getApi('/datalogger/compute3', { in: $scope.inWindow, out: $scope.outWindow }, (d) => {
      $scope.score = d.data;
    });
  };

  $scope.applyCompute = () => {
    $scope.computeResultIntersection = _.intersection($scope.computeResult, $scope.compute2Result);
  };

  $scope.applyComputeToChart = () => {
    _.each($scope.computeResultIntersection, (e) => {
      getApi('/datalogger/values', { name: e }, (d) => {
        const serie = {
          turboThreshold: 10000,
          id: e,
          name: e,
          yAxis: 0,
          data: _.map(d.data, o => ({
            x: Date.parse(o.x), // moment(o.x).toDate(),
            y: parseInt(o.y, 10),
            ts: moment(o.x).valueOf(),
          })),
          marker: {
            enabled: false,
          },
        };
        $scope.chart.addSeries(serie);
      });
    });
  };


  $scope.validFilter = (e) => {
    if (e.keyCode === 13 && $scope.filteredTags.length > 0) {
      $scope.add($scope.filteredTags[0], false);
    }
  };

  $scope.addEventToGraph = (event) => {
    if (!$scope.chart.get(event.name)) {
      let val = 0; // 29
      const dataSerie = [];
      _.each(event.Events, (e) => {
        dataSerie.push({
          x: Date.parse(e.timestamp),
          y: val,
          ts: moment(e.timestamp).valueOf(),
        });
        val += parseInt(e.value, 10);
      });

      const serie = {
        turboThreshold: 10000,
        id: event.name,
        name: event.name,
        yAxis: 0,
        data: dataSerie,
        marker: {
          enabled: true,
        },
        dashStyle: 'ShortDash',
      };
      $scope.chart.addSeries(serie);
    }
  };


  $scope.windows = {
    in: [],
    out: [],
  };

  $scope.createMask = (events, offset = 0) => {
    const ref = $scope.images;
    const serie = [];
    $scope.windows.ref = events;
    $scope.windows.in = [];
    $scope.windows.out = [];

    // Création d'un signal toujours à 0 avec ajout des événements relatifs au point
    _.each(ref, (e) => {
      const ev = _.findWhere(events.Events, { timestamp: e.timestamp });
      serie.push({
        x: Date.parse(e.timestamp),
        y: 0,
        color: (ev) ? '#00FF00' : null,
        relatedEvent: ev,
        valueSrc: 'initial',
      });
    });

    // Création des valeurs à 1 autour des événements (selon la vaeur de l'offset)
    _.each(serie, (e, index) => {
      if (e.relatedEvent) {
        const start = (index - offset < 0) ? 0 : index - offset;
        const stop = (index + offset > serie.length) ? serie.length : index + offset;

        for (let j = start; j < stop; j += 1) {
          serie[j].y = 1;
          serie[j].valueSrc = 'modified';
        }
      }
    });

    // Création de la variable avec les débuts et fins des fenêtres
    let memDate = serie[0].x;
    let memIndex = 0;

    for (let i = 1; i < serie.length; i += 1) {
      if (serie[i - 1].y === 0 && serie[i].y === 1) {
        $scope.windows.out.push({
          start: memDate,
          startIndex: memIndex,
          end: serie[i].x,
          endIndex: i,
        });
        memDate = serie[i].x;
        memIndex = i;
      } else if (serie[i - 1].y === 1 && serie[i].y === 0) {
        $scope.windows.in.push({
          start: memDate,
          startIndex: memIndex,
          end: serie[i].x,
          endIndex: i,
        });
        memDate = serie[i].x;
        memIndex = i;
      }
    }

    if (serie[serie.length - 1] === 0) {
      $scope.windows.out.push({
        start: memDate,
        startIndex: memIndex,
        end: serie[serie.length - 1].x,
        endIndex: serie.length - 1,
      });
    } else {
      $scope.windows.in.push({
        start: memDate,
        startIndex: memIndex,
        end: serie[serie.length - 1].x,
        endIndex: serie.length - 1,
      });
    }


    if ($scope.chart.get(`mask-${events.name}`)) $scope.chart.get(`mask-${events.name}`).remove();
    $scope.chart.addSeries({
      turboThreshold: 10000,
      id: `mask-${events.name}`,
      name: `Mask ${events.name}`,
      yAxis: 1,
      type: 'area',
      data: serie,
      marker: {
        enabled: true,
      },
      zIndex: -1,
      tooltip: {
        pointFormatter() {
          return `</br><b>Date :</b> ${moment(this.x, 'xx').format('HH:mm:ss.SSS')}</br><b>Event :</b> ${this.relatedEvent}`;
        },
        useHTML: true,
      },
    });
    $scope.mask = serie;
    return serie;
  };


  $scope.deriveToGraph = (datalogger) => {
    const dt = datalogger;
    dt.visible = true;
    if (!$scope.chart.get(datalogger.name)) {
      getApi('/datalogger/values', { name: datalogger.name }, (d) => {
        const serie = {
          turboThreshold: 10000,
          yAxis: 1,
          id: `derive-${datalogger.name}`,
          name: `derive-${datalogger.name}`,
          data: _.map(d.data, o => ({
            x: Date.parse(o.x), // moment(o.x).toDate(),
            y: parseInt(o.y2, 10),
            ts: moment(o.x).valueOf(),
          })),
          marker: {
            enabled: false,
          },
        };
        $scope.chart.addSeries(serie);
      });
    }
  };

  $scope.add = (datalogger, ref) => {
>>>>>>> develop
    const dt = datalogger;
    dt.visible = true;
    if (!$scope.chart.get(datalogger.name)) {
      getApi('/datalogger/values', { name: datalogger.name }, (d) => {
<<<<<<< HEAD
        const serie = $scope.chart.addSeries({
          id: datalogger.name,
          name: datalogger.name,
          data: _.map(d.data, o => ({ x: Date.parse(o.x), y: parseInt(o.y, 10) })),
        });
        dt.color = serie.color;
=======
        const serie = {
          turboThreshold: 10000,
          id: datalogger.name,
          name: datalogger.name,
          yAxis: 0,
          data: _.map(d.data, o => ({
            x: Date.parse(o.x), // moment(o.x).toDate(),
            y: ref ? parseInt(0, 10) : parseInt(o.y, 10),
            ts: moment(o.x).valueOf(),
          })),
          marker: {
            enabled: false,
          },
        };
        if (ref) serie.marker.enabled = true;
        const serieAdded = $scope.chart.addSeries(serie);
        dt.color = serieAdded.color;
        dt.ref = ref;
        if (ref) $scope.ref = true;
      });
    }
  };


  $scope.signals = [];
  $scope.valueSignal = [];
  $scope.loadingStatut = 0;
  // dt = données d'un tag
  $scope.toDerived = (dt) => {
    _.each(dt, (datalogger) => {
      const dbis = getApiSync('/datalogger/values', { name: datalogger.name });
      dbis.then((e) => {
        const data = [];
        let previous = parseInt(e.data[0].y, 10);

        _.each(e.data, (o) => {
          data.push({
            x: Date.parse(o.x), // moment(o.x).toDate(),
            y: (previous === parseInt(o.y, 10)) ? 0 : 1,
            ts: moment(o.x).valueOf(),
          });
          const dlUpdated = {
            name: datalogger.name,
            tsRef: Date.parse(o.x), // moment(o.x).toDate(),
            value: o.y,
            derivate: (previous === parseInt(o.y, 10)) ? 0 : 1,
            ts: moment(o.x).valueOf(),
          };
          $scope.valueSignal.push(dlUpdated);
          // db.dl.upsert(dlUpdated);
          previous = parseInt(o.y, 10);
        });
        $scope.signals.push(_.extend({
          data: e.data,
          derivate: data,
        }, datalogger));
        $scope.loadingStatut = Math.round($scope.signals.length / dt.length * 100);
      });
    });
  };

  $scope.c2 = () => {
    getApi('/datalogger/formatDataUpdate', {}, (q) => {
      _.reduce(q.data, (memo, num) => memo + num, '');
    });
  };

  $scope.c = () => {
    console.log('signals', $scope.signals);
    console.log('valueSignal', $scope.valueSignal);
    console.log('window', $scope.windows);
    // getApiSync('/datalogger/formatData', {});

    // putApi('/datalogger/', $scope.valueSignal, () => {
    //   console.log('updated');
    // });
    // db.dl.aggregate([
    //   {
    //     $match: {
    //       tsRef: {
    //         $gte: Date.parse('2018-10-26T13:19:16.183Z'),
    //         $lte: Date.parse('2018-10-26T13:20:16.183Z'),
    //       },
    //     },
    //     $group: {
    //       name: 'name',
    //       total: { $sum: '$derivate' },
    //     },
    //   },
    // ], {}).fetch((res) => {
    //   console.log('minimongo', res);
    // });
  };

  function getScore(txt) {
    const values = txt.split(',');
    return _.reduce(values, (memo, num, idx, list) => {
      if (idx === 0) return 0;
      if (list[idx - 1] !== num) {
        return memo + 1;
      }
      return parseInt(memo, 10);
    }, 0);
  }

  function testSignal(window) {
    // Récupération des valeurs sur la fenêtre en cours
    getApi('/datalogger/valuesConcat', {
      start: window.start,
      end: window.end,
    }, (results) => {
      _.each(results.data, (result) => {
        window.signals.push({
          name: result.name,
          qty: getScore(result.value),
          value: result.value,
        });
      });
    });
  }

  function getEventsQty(window) {
    getApi('/events/windowQty', {
      start: window.start,
      end: window.end,
      id: $scope.windows.ref.id,
    }, (e) => {
      window.events = e.data[0].qty || -1;
    });
  }

  function orderByScore() {
    $scope.windows.result = {};
    $scope.windows.resultFinal = [];
    $scope.windows.inResultFinal = [];
    $scope.windows.outResultFinal = [];
    $scope.windows.inResultSorted = [];
    $scope.windows.outResultSorted = [];

    // mise à zéro
    _.each($scope.windows.in, (window) => {
      _.each(window.signals, (signal) => {
        if (signal.name !== undefined) {
          $scope.windows.result[signal.name] = {
            in: { signalQty: 0 },
            out: { signalQty: 0 },
          };
        }
      });
    });


    // Parcours de chaque fenêtre
    _.each($scope.windows.in, (window) => {
      _.each(window.signals, (signal) => {
        if (signal.name !== undefined) {
          $scope.windows.result[signal.name].in.signalQty += signal.qty / window.events;
        }
      });
    });

    _.each($scope.windows.out, (window) => {
      _.each(window.signals, (signal) => {
        if (signal.name !== undefined) {
          const diff = moment.duration(moment(window.end).diff(moment(window.start)));
          window.start, window.end, signal.qty, diff.asSeconds();
          $scope.windows.result[signal.name].out.signalQty += signal.qty * 100 / diff.asSeconds();
        }
      });
    });

    // Regroupement dans un tableau d'objets
    _.each($scope.windows.result, (result, idx) => {
      const inScore = result.in.signalQty / $scope.windows.in.length;
      const outScore = result.out.signalQty / $scope.windows.out.length;
      $scope.windows.resultFinal.push({
        name: idx,
        in: {
          signalQty: result.in.signalQty,
          score: inScore,
          windowsQty: $scope.windows.in.length,
        },
        out: {
          signalQty: result.out.signalQty,
          score: outScore,
          windowsQty: $scope.windows.in.length,
        },
      });

      if (inScore > 0) {
        $scope.windows.inResultFinal.push({
          name: idx,
          score: inScore,
        });
      }

      $scope.windows.outResultFinal.push({
        name: idx,
        score: outScore,
      });
    });

    const maxValue = _.max($scope.windows.resultFinal, e => e.out.score);

    _.map($scope.windows.resultFinal, (e) => {
      return _.extend(e, { score: Math.abs(1 - e.in.score * (1 - (e.out.score / maxValue.out.score))) });
    })

    $scope.windows.max = maxValue.out.score;

    // Tri
    $scope.windows.inResultSorted = _.sortBy($scope.windows.resultFinal, 'in.score');
    $scope.windows.outResultSorted = _.sortBy($scope.windows.resultFinal, 'out.score');
    $scope.windows.inResultFinal = _.sortBy($scope.windows.inResultFinal, 'score');
    $scope.windows.outResultFinal = _.sortBy($scope.windows.outResultFinal, 'score');

    // containerResult chartResult

    // const dataIn = [];
    // const dataOut = [];
    // _.each($scope.windows.inResultFinal, (o) => {
    //   dataIn.push([o.name, o.score]);
    // });
    //
    // _.each($scope.windows.outResultFinal, (o) => {
    //   dataOut.push([o.name, o.score]);
    // });
    //
    // $scope.chartResult.addSeries({
    //   name: 'In',
    //   data: dataIn,
    //   yAxis: 0,
    // });
    // $scope.chartResult.addSeries({
    //   name: 'Out',
    //   data: dataOut,
    //   yAxis: 1,
    // });

    const data = [];
    _.each($scope.windows.resultFinal, (r) => {
      data.push([
        [r.name, r.in.score],
        [r.name, r.out.score],
      ]);
    });
    // $scope.chartResult.addSeries({ data });
  }

  //

  $scope.finalScore = () => {
    orderByScore();
  };


  $scope.filterWindow = () => {
    _.each($scope.windows.in, (d, idx) => {
      d.signals = [idx];
      d.events = [idx];

      testSignal(d);
      getEventsQty(d);
    });


    _.each($scope.windows.out, (d, idx) => {
      d.signals = [idx];
      d.events = [idx];

      testSignal(d);
      getEventsQty(d);
    });
  };


  $scope.derive = (datalogger) => {
    const dt = datalogger;
    dt.visible = false;
    if (!$scope.chart.get(datalogger.name)) {
      getApi('/datalogger/values', { name: datalogger.name }, (d) => {
        const data = [];
        let previous = parseInt(d.data[0].y, 10);

        _.each(d.data, (o) => {
          data.push({
            x: Date.parse(o.x), // moment(o.x).toDate(),
            y: (previous === parseInt(o.y, 10)) ? 0 : 1,
            ts: moment(o.x).valueOf(),
          });
          previous = parseInt(o.y, 10);
        });

        const serie = {
          turboThreshold: 10000,
          id: `derive-${datalogger.name}`,
          name: `derive-${datalogger.name}`,
          yAxis: 1,
          data,
          marker: {
            enabled: false,
          },
          derive: true,
        };
        const serieAdded = $scope.chart.addSeries(serie);
        dt.color = serieAdded.color;
>>>>>>> develop
      });
    }
  };

  $scope.del = (datalogger) => {
    const dt = datalogger;
    dt.visible = false;
    dt.color = undefined;
<<<<<<< HEAD
    $scope.chart.get(datalogger.name).remove();
  };

  $scope.start = (qty) => {
    getUdp(qty);
  };
=======
    if (dt.ref) $scope.ref = false;
    if (dt.derive) {
      $scope.chart.get(`derive-${dt.name}`).remove();
    } else {
      $scope.chart.get(dt.name).remove();
    }
  };

  $scope.stop = () => {
    $scope.socket.emit('stopSnap');
  };

  $scope.start = (qty, interval) => {
    $scope.qty = qty;
    $scope.socket.emit('startSnap', {
      qty,
      interval,
    });
  };

  $scope.propertyName = 'in.score';
  $scope.reverse = true;

  $scope.sortBy = (propertyName) => {
    $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
    $scope.propertyName = propertyName;
  };

  $scope.changeMode = (state) => {
    $scope.mode = state;
  };

  $scope.createEvent = (el, ev) => postApi(`/eventslist/${el}/events`, {
    value: ev.value,
    timestamp: ev.timestamp,
  }, () => {
    $scope.addEvent = {};
    $scope.applyEventsList(el);
    updateEventsForImage();
    // $scope.$apply();
  });

  $scope.imagePrevious = (nb) => {
    if ($scope.index > 0) {
      $scope.index -= nb;
      updateEventsForImage();
    }
  };

  $scope.imageNext = (nb) => {
    $scope.index += nb;
    updateEventsForImage();
  };

  $scope.deleteEvent = id => deleteApi(`/eventslist/events/${id}`, {}, () => {
    // $scope.applyEventsList($scope.selectEventsList);
    updateEventsForImage();
    // $scope.$apply();
  });

  $scope.applyEventsList = el => getApi(`/eventslist/${el}`, {}, (d) => {
    $scope.currentEventsList = d.data;

    if ($scope.chart.get(`ev-${d.data.id}`)) $scope.chart.get(`ev-${d.data.id}`).remove();

    $scope.chart.addSeries({
      id: `ev-${d.data.id}`,
      name: d.data.name,
      yAxis: 0,
      data: _.map(d.data.Events, o => ({ x: Date.parse(o.timestamp), y: 0, name: o.value })),
    });
  });
>>>>>>> develop
});
