module.exports = {
    env: {
        browser: true,
        es6: true,
        node: true,
        jquery: true
    },
    extends: [
      "airbnb"
    ],
    parserOptions: {
        ecmaFeatures: {
            jsx: true
        },
        ecmaVersion: 2015,
        sourceType: "module"
    },
    rules: {
      "linebreak-style": 0,
      "no-trailing-spaces": 0,
      "indent": ["error", 2, { "MemberExpression": 0 }]
    }
}
