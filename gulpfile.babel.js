const gulp = require('gulp');
const browserify = require('browserify');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const babelify = require('babelify');
const nodemon = require('gulp-nodemon');
const browserSync = require('browser-sync').create();
const { exec } = require('child_process');

// Nodemon task
gulp.task('node', (cb) => {
  let started = false;

  return nodemon({
    script: './bin/www',
  })
  .on('start', () => {
    if (!started) {
      cb();
      started = true;
    }
  });
});

// Build task
gulp.task('build', ['js']);


// Compile Javascript
gulp.task('js', () => {
  browserify({
    entries: ['src/index.js', 'src/results.js'],
    debug: true,
  })
  .transform(babelify)
  .bundle()
  .pipe(source('core.js'))
  .pipe(buffer())
  .pipe(gulp.dest('./public/js'));
});

// Watch task
gulp.task('watch', () => {
  nodemon({
    script: './bin/www',
    watch: 'app.js',
    tasks: ['build'],
  });
});

// Watch javascript task
gulp.task('js-watch', ['js'], (done) => {
  browserSync.reload();
  done();
});

gulp.task('browser-sync', ['node'], () => {
  browserSync.init(null, {
    proxy: 'http://localhost:3000',
    files: ['public/**/*.*'],
    browser: 'chrome',
    port: 4000,
    ui: {
      port: 3003,
    },
  });
});


// Default task
gulp.task('default', ['build', 'browser-sync'], () => {
  exec('C:\\xampp\\mysql_start.bat');
  exec('C:\\xampp\\apache_start.bat');
  gulp.watch('src/*.js', ['js-watch']);
});
