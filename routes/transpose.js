const Sequelize = require('sequelize');
const express = require('express');
const moment = require('moment');
const _ = require('underscore');

const router = express.Router();
const { Datalogger, sequelize } = require('../models');

const { Op } = Sequelize;
let Transpose;

/* plus utilisé dans la dernière version */

function createModel() {
  Datalogger.findAll({
    attributes: ['name'],
    group: ['name'],
  }).then((dataloggers) => {
    const columns = {};

    _.each(dataloggers, (c) => {
      columns[c.name] = Sequelize.INTEGER;
    });

    columns.id = {
      type: Sequelize.DATE(3),
      primaryKey: true,
      autoIncrement: false,
    };
    Transpose = sequelize.define('Transpose', columns, {
      timestamps: false,
    });
    Transpose.sync({ force: true });
  });
}

function addValues(ts) {
  Datalogger.findAll({
    attributes: ['name', 'value'],
    where: {
      timestamp: {
        [Op.gte]: moment(ts).subtract(100, 'ms').valueOf(),
        [Op.lte]: moment(ts).add(100, 'ms').valueOf(),
      },
    },
  })
  .then((result) => {
    const columns = { id: ts };

    _.each(result, (e) => {
      columns[e.name] = parseInt(e.value, 10);
    });

    Transpose.create(columns);
  });
}

function hydrateTable() {
  Datalogger.findAll({
    attributes: ['timestamp'],
    where: {
      name: 'Channel1.Device1.o',
    },
    group: ['timestamp'],
  }).then((ts) => {
    _.each(ts, (e) => {
      addValues(e.timestamp);
    });
  });
}


const tsArray = [];
const signalArray = [];

function getTransposed() {
  Datalogger.findAll({
    attributes: ['timestamp'],
    where: {
      name: 'Channel1.Device1.o',
    },
    group: ['timestamp'],
  }).then((tss) => {
    _.each(tss, (ts) => {
      Datalogger.findAll({
        attributes: ['name', 'value'],
        where: {
          timestamp: {
            [Op.gte]: moment(ts.timestamp).subtract(100, 'ms').valueOf(),
            [Op.lte]: moment(ts.timestamp).add(100, 'ms').valueOf(),
          },
        },
      })
      .then((values) => {
        tsArray.push(ts.timestamp);
        _.each(values, (value) => {
          signalArray[ts.timestamp] = {
            name: value.name,
            value: parseInt(value.value, 10),
            ts: ts.timestamp,
          };
        });
      });
    });
    return { ts: tsArray, signal: signalArray };
  });
}

router.get('/create', (req, res) => {
  createModel();
  res.json({ result: true });
});

router.get('/transposed', (req, res) => {
  res.json(getTransposed());
});

router.get('/transposedResult', (req, res) => {
  res.json({
    ts: tsArray,
    signal: signalArray,
  });
});

router.get('/transposedResultSignal', (req, res) => {
  res.json({ signalArray });
});

router.get('/transposedResultTs', (req, res) => {
  res.json({ tsArray });
});


router.get('/hydrate', (req, res) => {
  hydrateTable();
  res.json({ result: true });
});

router.get('/', (req, res) => {
  Transpose.findAll({}).then(() => {
    res.json({ result: true });
  });
});

module.exports = router;
