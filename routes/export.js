const Sequelize = require('sequelize');
const _ = require('underscore');
const express = require('express');
const moment = require('moment');
const bluebird = require('bluebird');

const router = express.Router();
const { sequelize, Datalogger, Events } = require('../models');


/* GET users listing. */
router.get('/', (req, res) => {

  // Get all timestamps
  Datalogger.findAll({
    attributes: ['timestamp'],
    group: ['timestamp'],
    raw: true,
  }).then((timestamps) => {
    const arr = {};
    let csv = 'timestamp,';
    let firstLine = true;
    return bluebird.map(timestamps, (t) => {
      return sequelize.query(
        `SELECT * FROM (SELECT name, value FROM dataloggers WHERE timestamp='${moment(t.timestamp).add(2, 'h').toISOString()}'  UNION ALL SELECT name, null as value FROM dataloggers GROUP BY name ) c GROUP BY c.name`,
        { type: Sequelize.QueryTypes.SELECT },
      ).then((v) => {
        arr[moment(t.timestamp).add(2, 'h').toISOString()] = v;

        if (firstLine) {
          _.each(v, (e => {
            csv = csv + e.name + ',';
          }));
          csv = csv.slice(0, -1);
          firstLine = false;
        }

        csv = csv + '\r\n' + moment(t.timestamp).add(2, 'h').toISOString() + ',';
        _.each(v, (e => {
          csv = csv + e.value + ',';
        }));
        csv = csv.slice(0, -1);
      });
    }).then(() => {
      console.log(csv);
      res.header("Content-Type", "text/csv");
      res.send(csv);
    });
  });
});

module.exports = router;
