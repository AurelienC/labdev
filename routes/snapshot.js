const dgram = require('dgram');
const { spawn } = require('child_process');
const path = require('path');

const PORT_HMI = 33333;
const PORT_CANCEL = 2102;
const HOST = '127.0.0.1';
const server = dgram.createSocket('udp4');

server.bind(PORT_HMI, HOST);

module.exports = (io) => {
  io.sockets.on('connection', (socket) => {
    /* Bouton "Arrêt" de l'IHM, on envoie une trame au prg Python pour arrêter */
    socket.on('stopSnap', () => {
      const message = new Buffer('stop');
      const client = dgram.createSocket('udp4');
      client.send(message, 0, message.length, PORT_CANCEL, HOST, (err) => {
        if (err) throw err;
        client.close();
      });
    });

    /* Bouton Démarrer de l'IHM, on lance le prg Python */
    socket.on('startSnap', (data) => {
      spawn('py', [path.resolve(__dirname, '..\\src\\snap.py'), data.qty, data.interval]);
    });
  });

  server.on('message', (message) => {
    io.sockets.emit('c', { mes: message.toString() });
  });
};
