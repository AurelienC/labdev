const Sequelize = require('sequelize');
const express = require('express');
const moment = require('moment');
const _ = require('underscore');
const fs = require('fs');

const router = express.Router();
const { Datalogger, sequelize } = require('../models');

const { Op } = Sequelize;

/* GET users listing. */
router.get('/', (req, res) => {
  Datalogger.findAll({
    where: req.query,
  }).then((dataloggers) => {
    res.json(dataloggers);
  });
});

/* GET démultiplication des mots en bits */
router.get('/wordtobits', (req, res) => {
  Datalogger.findAll({
    attributes: [
      'timestamp',
      'value',
      'derivate',
      'name',
    ],
    where: {
      name: req.query.name,
    },
    order: [
      ['timestamp', 'ASC'],
    ],
  }).then((dataloggers) => {
    _.forEach(dataloggers, (dt) => {
      for (let i = 0; i < 16; i += 1) {
        Datalogger.create({
          name: `${dt.name}.${i}`,
          timestamp: dt.timestamp,
          value: (dt.value >> i) & 1, // eslint-disable-line no-bitwise
        });
      }
    });
  });

  res.json({ result: 200, name: req.query.name });
});

/* GET récupération des tags et du nb de valeurs */
router.get('/tags', (req, res) => {
  Datalogger.findAll({
    attributes: ['name', [Sequelize.fn('COUNT', 'name'), 'qty']],
    group: ['name'],
  }).then((dataloggers) => {
    res.json(dataloggers);
  });
});

/* GET valeurs des tags */
router.get('/values', (req, res) => {
  Datalogger.findAll({
    attributes: [
      ['timestamp', 'x'],
      ['value', 'y'],
      ['derivate', 'y2'],
      'name',
      'id',
    ],
    where: req.query,
    order: [
      ['timestamp', 'ASC'],
    ],
  }).then((dataloggers) => {
    res.json(dataloggers);
  });
});

/* GET récupération des valeurs concatainées d'un tag */
router.get('/valuesConcat', (req, res) => {
  Datalogger.findAll({
    attributes: [
      'name',
      [sequelize.literal('GROUP_CONCAT(value ORDER BY timestamp)'), 'value'],
    ],
    where: {
      timestamp: {
        [Op.gte]: moment(new Date(parseInt(req.query.start, 10))).subtract(100, 'ms').valueOf(),
        [Op.lte]: moment(new Date(parseInt(req.query.end, 10))).add(100, 'ms').valueOf(),
      },
    },
    order: [
      ['timestamp', 'ASC'],
    ],
    group: ['name'],
  }).then((dataloggers) => {
    res.json(dataloggers);
  });
});

/* GET récupération des valeurs */
router.get('/window', (req, res) => {
  Datalogger.findAll({
    attributes: ['name', 'value'],
    where: {
      timestamp: {
        [Op.gte]: moment(new Date(parseInt(req.query.start, 10))).subtract(100, 'ms').valueOf(),
        [Op.lte]: moment(new Date(parseInt(req.query.end, 10))).add(100, 'ms').valueOf(),
      },
    },
    order: [
      ['timestamp', 'ASC'],
    ],
  })
  .then((result) => {
    res.json(result);
  });
});

/* liste des tags qui sont tjs à zéro */
router.get('/zero', (req, res) => {
  Datalogger.findAll({
    attributes: ['name'],
    group: ['name'],
    having: sequelize.where(sequelize.fn('SUM', sequelize.col('value')), '=', 0),
  }).then((dataloggers) => {
    res.json(dataloggers);
  });
});

/* algo v1 */
router.get('/compute', (req, res) => {
  Datalogger.findAll({
    attributes: ['name'],
    where: {
      timestamp: {
        [Op.gte]: moment(new Date(parseInt(req.query.start, 10))).subtract(100, 'ms').valueOf(),
        [Op.lte]: moment(new Date(parseInt(req.query.end, 10))).add(100, 'ms').valueOf(),
      },
      name: {
        [Op.in]: req.query.signalsInTheRace,
        [Op.like]: '%MW%',
      },
    },
    group: ['name'],
    having: sequelize.where(sequelize.fn('COUNT', sequelize.fn('DISTINCT', sequelize.col('value'))), '<', 4),
  }).then((dataloggers) => {
    res.json(dataloggers);
  });
});

/* algo v2 */
router.get('/compute2', (req, res) => {
  Datalogger.findAll({
    attributes: ['name'],
    where: {
      timestamp: {
        [Op.gte]: moment(new Date(parseInt(req.query.start, 10))).subtract(100, 'ms').valueOf(),
        [Op.lte]: moment(new Date(parseInt(req.query.end, 10))).add(100, 'ms').valueOf(),
      },
      name: {
        [Op.in]: req.query.signalsInTheRace,
        [Op.like]: '%MW%',
      },
    },
    group: ['name'],
    having: sequelize.where(sequelize.fn('COUNT', sequelize.fn('DISTINCT', sequelize.col('value'))), '>', 1),
  }).then((dataloggers) => {
    res.json(dataloggers);
  });
});

/* algo v3 */
router.get('/compute3', (req, res) => {
  const whereIn = [];
  const whereOut = [];

  _.each(req.query.in, (e) => {
    const eJson = JSON.parse(e);
    whereIn.push({
      [Op.gte]: moment(new Date(parseInt(eJson.start, 10))).subtract(100, 'ms').valueOf(),
      [Op.lte]: moment(new Date(parseInt(eJson.end, 10))).add(100, 'ms').valueOf(),
    });
  });

  _.each(req.query.out, (e) => {
    const eJson = JSON.parse(e);
    whereOut.push({
      [Op.gte]: moment(new Date(parseInt(eJson.start, 10))).subtract(100, 'ms').valueOf(),
      [Op.lte]: moment(new Date(parseInt(eJson.end, 10))).add(100, 'ms').valueOf(),
    });
  });

  Datalogger.findAll({
    attributes: [
      'name',
      [sequelize.fn('COUNT', sequelize.fn('DISTINCT', sequelize.col('value'))), 'score'],
    ],
    where: {
      timestamp: {
        [Op.or]: whereIn,
      },
      name: {
        [Op.like]: '%MW%',
      },
    },
    group: ['name'],
    // having: sequelize.where(sequelize.fn('VARIANCE', sequelize.col('value')), '<', 2),
  }).then((dataloggersIn) => {
    Datalogger.findAll({
      attributes: [
        'name',
        [sequelize.fn('COUNT', sequelize.fn('DISTINCT', sequelize.col('value'))), 'score'],
      ],

      where: {
        timestamp: {
          [Op.or]: whereOut,
        },
        name: {
          [Op.like]: '%MW%',
        },
      },
      group: ['name'],
    }).then((dataloggersOut) => {
      const result = [];
      _.each(dataloggersIn, (i) => {
        const o = _.where(dataloggersOut, { name: i.name });

        let oScore = 0;
        if (o.length > 0) {
          oScore = o[0].dataValues.score;
        }

        result.push({
          name: i.name,
          score: i.dataValues.score - oScore,
        });
      });
      res.json({ in: dataloggersIn, out: dataloggersOut, score: result });
    });
  });
});

/* récupération nom image */
router.get('/image', (req, res) => {
  const dt = new Date(parseInt(req.query.timestamp, 10));
  Datalogger.findAll({
    attributes: [
      ['value', 'y'],
    ],
    where: {
      timestamp: {
        [Op.gte]: moment(dt).subtract(50, 'ms').valueOf(),
        [Op.lte]: moment(dt).add(50, 'ms').valueOf(),
      },
      name: 'Channel1.Device1.o',
    },
    limit: 1,
  }).then((dataloggers) => {
    res.json(dataloggers);
  });
});

/* récupération des images sur une plage */
router.get('/images', (req, res) => {
  const where = { name: 'Channel1.Device1.o' };
  if (req.query.start && req.query.end) {
    _.extend(where, {
      timestamp: {
        [Op.gte]: new Date(req.query.start).valueOf(),
        [Op.lte]: new Date(req.query.end).valueOf(),
      },
    });
  }
  Datalogger.findAll({
    attributes: [
      ['value', 'img'],
      'timestamp',
    ],
    where,
    order: [
      ['timestamp', 'ASC'],
    ],
  }).then((dataloggers) => {
    res.json(dataloggers);
  });
});

/* PUT mise à jour dérivée */
router.put('/:id', (req) => {
  const { id } = req.params;
  _.each(req.params, (e) => {
    Datalogger.update({
      ref: e.tsRef,
      derivate: e.derivate,
    }, {
      where: { id },
      returning: true,
    });
  });
});

module.exports = router;
