const Sequelize = require('sequelize');
const express = require('express');
const moment = require('moment');
// const _ = require('underscore');

const router = express.Router();
const { Events, EventsList } = require('../models');

const { Op } = Sequelize;

/* GET tout */
router.get('/', (req, res) => {
  Events.findAll({
    where: req.query,
    order: [
      ['timestamp', 'ASC'],
    ],
    include: [EventsList],
  })
  .then((result) => {
    res.json(result);
  });
});

/* GET évènements dans une fenêtre */
router.get('/window', (req, res) => {
  Events.findAll({
    where: {
      timestamp: {
        [Op.gte]: moment(new Date(parseInt(req.query.start, 10))).subtract(100, 'ms').valueOf(),
        [Op.lte]: moment(new Date(parseInt(req.query.end, 10))).add(100, 'ms').valueOf(),
      },
    },
    order: [
      ['timestamp', 'ASC'],
    ],
    include: { EventsList },
  })
  .then((result) => {
    res.json(result);
  });
});

/* GET nb d'evt dans une fenêtre */
router.get('/windowQty', (req, res) => {
  Events.findAll({
    attributes: [[Sequelize.fn('COUNT', '1'), 'qty']],
    where: {
      timestamp: {
        [Op.gte]: moment(new Date(parseInt(req.query.start, 10))).subtract(100, 'ms').valueOf(),
        [Op.lte]: moment(new Date(parseInt(req.query.end, 10))).add(100, 'ms').valueOf(),
      },
    },
    order: [
      ['timestamp', 'ASC'],
    ],
    include: [{
      model: EventsList,
      where: { id: req.query.id },
    }],
  })
  .then((result) => {
    res.json(result);
  });
});

/* GET un évènement spécifique */
router.get('/:id', (req, res) => {
  const { id } = req.params;

  Events.find({
    where: { id },
    order: [
      ['timestamp', 'ASC'],
    ],
  })
  .then((result) => {
    res.json(result);
  });
});


module.exports = router;
