const Sequelize = require('sequelize');
const express = require('express');
// const _ = require('underscore');

const router = express.Router();
const { EventsList, Events } = require('../models');

/* GET tout */
router.get('/', (req, res) => {
  EventsList.findAll({
    include: [Events],
    order: [
      [Events, 'timestamp', 'ASC'],
    ],
  })
  .then((result) => {
    res.json(result);
  });
});

/* GET liste spécifique */
router.get('/:id', (req, res) => {
  const { id } = req.params;

  EventsList.find({
    where: { id },
    order: [
      [Events, 'timestamp', 'ASC'],
    ],
    include: [Events],
  })
  .then((result) => {
    res.json(result);
  });
});

/* GET évènements d'une liste */
router.get('/:id/events', (req, res) => {
  const { id } = req.params;

  EventsList.find({
    where: { id },
    order: [
      ['timestamp', 'ASC'],
    ],
  })
  .then((el) => {
    el.getEvents()
    .then((events) => {
      res.json(events);
    });
  });
});

/* POST une liste */
router.post('/', (req, res) => {
  const { name, type } = req.body.params;

  EventsList.create({
    name,
    type,
  })
  .then(() => {
    res.json(req.body);
  });
});

/* POST un évènement d'une liste */
router.post('/:id/events', (req, res) => {
  const { value, timestamp } = req.body.params;
  const eventslistid = req.params.id;

  EventsList.find({
    where: { id: eventslistid },
  })
  .then((eventsList) => {
    if (!eventsList) {
      res.status(400).send('No EventsList with this id.');
    } else {
      Events.create({
        value,
        timestamp,
      })
      .then((events) => {
        events.setEventsList(eventsList)
        .then(() => {
          res.json(events);
        });
      });
    }
  });
});

/* DELETE une liste */
router.delete('/:id', (req, res) => {
  const { id } = req.body;

  EventsList.destroy({
    where: { id },
  })
  .then((del) => {
    res.json(del);
  });
});

/* DELETE un evt d'une liste */
router.delete('/events/:id', (req, res) => {
  const { id } = req.params;

  Events.destroy({
    where: { id },
  })
  .then((del) => {
    res.json(del);
  });
});


module.exports = router;
