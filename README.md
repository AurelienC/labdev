# Principe
Cet outil a pour but de permettre d'identifier des variables automates correspondant à des états physiques, discriminées par des événements déclarés manuellement sur des photo prises automatiquement lors de l'enregistrement.
L'outil prend des photos de la machine à des intervals fixes et récupère, à chaque photo, la mémoire de l'automate. L'utilisateur analyse ensuite les photos et déclare des événements lorsque l'état physique recherché évolue.
L'alhorithme est lancé et cherche des corrélations entre les événements déclarés et les évolutions des variables. Il affiche ensuite deux résultats permettant à l'utilisateur d'identifier la variable représentant l'état physique recherché.


# Etapes
Différentes étapes pour réaliser ceci :
1. Installer le matériel (PC avec cet outil et un serveur OPC)
2. Relier le serveur OPC à l'automate et renseigner les plages mémoires de l'automate dans le serveur
3. Fixer une webcam (connexion USB) permettant de visualiser la machine
4. Définir le temps entre chaque enregistrement et le nombre d'enregistrement
5. Lancer l'enregistrement
6. Définir une largeur pour les fenêtres et le début/fin de l'enregistrement à analyser
7. Analyser manuellement les photos et déclarer des événements
8. Lancer l'algorithme
9. Exploiter les résultats

# Logiciels et matériel
L'interface Homme/machine est développée en Javascript avec le serveur NodeJS et se lance avec Gulp. L'algorithme est lui aussi en JS.
La base de donnée est en MySQL (ici avec XAMPP).
La prise de photo se fait avec un programme en Python.
La récupération des données se fait avec un serveur OPC et un module (ici Datalogger de KEPServerEX) pour exporter les données dans une base de données.

# Manipulations


# Détails techniques
## IHM
Le lancement se fait avec Gulp, avec la commande `gulp` à la racine du dossier. Les détails du lancement sont dans le fichier `gulpfile.babel.js`.
Certains éléments sont statiques, et Gulp s'attend à trouver les éléments suivants, pour lancer la base de donnée et le serveur HTTP (utile uniquement pour phpMyAdmin).
```
C:\\xampp\\mysql_start.bat'
C:\\xampp\\apache_start.bat'
```

## Prises de vue
Le programme Python récupère en général la webcam et ignore les caméras USB connectés, `cam = cv2.VideoCapture(0)`.

## Base de données
Les identifiants de la base de données sont contenu dans le fichier `models\index.js` et sont :
* nom de la base : user1
* identifiant : user1
* mot de passe : user1

Les tables de la base de données sont construites si inexistantes, cependant, l'IHM pour ajouter des types d'événements n'a pas été créée, il faut donc au moins créer un type d'événement à la main, via phpMyAdmin par exemple, dans la table `eventslists`.

## Serveur OPC
Les fichiers de configuration du serveur OPC sont `kepserver[123].opf` et s'utilisent avec KEPServerEX, le module U-CON et Datalogger.
Le programme Python envoie une trame TCP au serveur OPC qui est traitée avec le module U-CON. A chaque changement de la valeur de cette variable (la trame met à jour la valeur d'une variable `Channel1.Device1.o` avec la date-heure de la prise de vue), le module DataLogger enregistre toutes les valeurs des variables dans la table `datalogger` de la base de données.
